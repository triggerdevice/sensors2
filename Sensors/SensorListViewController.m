//
//  SensorListViewController.m
//  Sensors
//
//  Created by Masahiro Murase on 2015/09/21.
//  Copyright (c) 2015年 TriggerDevice. All rights reserved.
//

#import "IASKSettingsReader.h"
#import "IASKAppSettingsViewController.h"

#import "AppDelegate.h"
#import "AppSettings.h"
#import "AppSettings+Import.h"
#import "MeshbluDevice.h"

#import "STDeviceManager.h"
#import "SensorTag/STSensorTag.h"

#import "SensorValueTransmitter.h"
#import "IDCFSubscriber.h"

#import "QRCodeReaderViewController.h"
#import "SensorTagSelectorViewController.h"
#import "SensorListViewController.h"

@interface SensorListViewController () <SensorTagSelectorViewControllerDelegate, IASKSettingsDelegate, QRCodeReaderViewControllerDelegate>
@property (nonatomic, strong) STDeviceManager *deviceManager;
@property (nonatomic, strong) NSTimer *sensorValueUpdateTimer;
@property (nonatomic, strong) NSDate *latestPostDate;
@property (nonatomic, weak) AppSettings *appSettings;
@property (nonatomic, strong) SensorValueTransmitter *transmitter;
@property (nonatomic, strong) IDCFSubscriber *idcfSubscriber;

// InAppSettingsKit
@property (nonatomic, strong) IASKAppSettingsViewController *appSettingsViewController;

// Actions & Outlets
@property (weak, nonatomic) IBOutlet UILabel *objctTemperatureLabel;
@property (weak, nonatomic) IBOutlet UILabel *ambientTemperatureLabel;
@property (weak, nonatomic) IBOutlet UILabel *accelerationXLabel;
@property (weak, nonatomic) IBOutlet UILabel *accelerationYLabel;
@property (weak, nonatomic) IBOutlet UILabel *accelerationZLabel;
@property (weak, nonatomic) IBOutlet UILabel *relativeHumidityLabel;
@property (weak, nonatomic) IBOutlet UILabel *pressureLabel;
@property (weak, nonatomic) IBOutlet UILabel *angularVelocityXLabel;
@property (weak, nonatomic) IBOutlet UILabel *angularVelocityYLabel;
@property (weak, nonatomic) IBOutlet UILabel *angularVelocityZLabel;
@property (weak, nonatomic) IBOutlet UILabel *magneticFieldXLabel;
@property (weak, nonatomic) IBOutlet UILabel *magneticFieldYLabel;
@property (weak, nonatomic) IBOutlet UILabel *magneticFieldZLabel;
@property (weak, nonatomic) IBOutlet UILabel *luminousIntensityLabel;
@property (weak, nonatomic) IBOutlet UILabel *userButtonLabel;
@property (weak, nonatomic) IBOutlet UILabel *powerButtonLabel;

- (IBAction)settingsButtonDidPush:(id)sender;

@end

@implementation SensorListViewController

- (void)awakeFromNib
{
  [super awakeFromNib];

  AppDelegate *appDelegate = [UIApplication sharedApplication].delegate;
  _appSettings = appDelegate.appSettings;

  _transmitter = [[SensorValueTransmitter alloc] initWithHost:_appSettings.host
                                                  triggerUUID:_appSettings.destinationTrigger.uuid
                                                 triggerToken:_appSettings.destinationTrigger.token
                                                      devices:_appSettings.forwardingActionUUIDs];
  _idcfSubscriber = [[IDCFSubscriber alloc] init];

  _latestPostDate = [NSDate date];

  
  _deviceManager = [[STDeviceManager alloc] init];
  [_deviceManager addObserver:self
                   forKeyPath:@"isReady"
                      options:NSKeyValueObservingOptionNew
                      context:NULL];
  [_deviceManager addObserver:self
                   forKeyPath:@"isConnected"
                      options:NSKeyValueObservingOptionNew
                      context:NULL];


}

- (void)viewDidLoad
{
  [super viewDidLoad];
  // Do any additional setup after loading the view, typically from a nib.
  
  _idcfSubscriber.host = _appSettings.host;
  _idcfSubscriber.userName = _appSettings.subscribeAction.uuid;
  _idcfSubscriber.password = _appSettings.subscribeAction.token;
  _idcfSubscriber.topic = _appSettings.subscribeAction.uuid;
  
  __weak typeof(self)  weakSelf = self;
  _idcfSubscriber.messageRecieverBlock = ^(NSData *payload){
    NSMutableDictionary *dic = [NSJSONSerialization JSONObjectWithData:payload
                                                               options:NSJSONReadingAllowFragments error:NULL];
    NSString *payloadString = dic[@"data"][@"payload"];
    [weakSelf.deviceManager.sensorTag applyIOMessage:payloadString];
  };
  if (_appSettings.isTransmissionEnabled) {
    [_idcfSubscriber subscribe];
  }
}

- (void)didReceiveMemoryWarning
{
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

#pragma mark - accessor

- (IASKAppSettingsViewController *)appSettingsViewController
{
  if (!_appSettingsViewController) {
    _appSettingsViewController = [[IASKAppSettingsViewController alloc] init];
    _appSettingsViewController.delegate = self;
  }
  return _appSettingsViewController;
}

#pragma mark - KVO callback

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
  if ([keyPath isEqualToString:@"isReady"]) {
    if ([change[NSKeyValueChangeNewKey] boolValue]) {
      dispatch_async(dispatch_get_main_queue(), ^{
        [self p_showSelector];
      });
    }
  }
  else if ([keyPath isEqualToString:@"isConnected"]) {
    dispatch_async(dispatch_get_main_queue(), ^{
      if ([change[NSKeyValueChangeNewKey] boolValue]) {
        [self p_startSensorValuesUpdate];
      }
      else {
        [self p_stopSensorValuesUpdate];
        [self p_showSelector];
      }
    });
  }
}

#pragma mark - Segues

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
  if ([segue.identifier isEqualToString:@"SensorTagSelector"]) {
    SensorTagSelectorViewController *vc = segue.destinationViewController;
    vc.delegate = self;
    vc.deviceManager = _deviceManager;
  }
}

#pragma mark - Private instance methods

- (void)p_startSensorValuesUpdate
{
  _sensorValueUpdateTimer = [NSTimer scheduledTimerWithTimeInterval:0.1
                                                             target:self
                                                           selector:@selector(p_updateTimerDidFire:)
                                                           userInfo:nil
                                                            repeats:YES];
}

- (void)p_stopSensorValuesUpdate
{
  [_sensorValueUpdateTimer invalidate];
  _sensorValueUpdateTimer = nil;
}

- (void)p_updateTimerDidFire:(NSTimer *)timer
{
  NSString *json = [_deviceManager.sensorTag takeJSONSnapshot];
  NSData *data = [json dataUsingEncoding:NSUTF8StringEncoding];
  NSDictionary *sensorValues = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:NULL];

  [self p_updateLabel:sensorValues];

  if (_appSettings.isTransmissionEnabled) {
    [self p_postSensorValueWithDictionary:sensorValues timer:timer];
  }
}

- (void)p_postSensorValueWithDictionary:(NSDictionary *)values timer:(NSTimer *)timer
{
  NSDate *nextPostDate = [NSDate dateWithTimeInterval:_appSettings.transmissionInterval
                                            sinceDate:_latestPostDate];

  BOOL postTimeHasCome = ([nextPostDate compare:timer.fireDate] == NSOrderedAscending);
  if (postTimeHasCome == YES) {
    _latestPostDate = [NSDate date];

    NSString *dateString = [NSDateFormatter localizedStringFromDate:_latestPostDate
                                                          dateStyle:NSDateFormatterNoStyle
                                                          timeStyle:NSDateFormatterMediumStyle];
    UIFont *font =  [UIFont fontWithName:@"AvenirNext-Regular" size:12.0f];
    __weak typeof(self) weakSelf = self;
    [_transmitter postSensorValues:values
                           success:^{
                             weakSelf.navigationController.navigationBar.titleTextAttributes =
                             @{
                               NSForegroundColorAttributeName: [UIColor lightGrayColor],
                               NSFontAttributeName: font,
                               };
                             weakSelf.title = [NSString stringWithFormat:@"POST success @ %@", dateString ];
                           }
                           failure:^(NSError *error){
                             weakSelf.navigationController.navigationBar.titleTextAttributes =
                             @{
                               NSForegroundColorAttributeName: [UIColor colorWithRed:0.7 green:0.2 blue:0.0 alpha:1.0],
                               NSFontAttributeName: font,
                               };
                             weakSelf.title = [NSString stringWithFormat:@"POST failure @ %@", dateString ];
                           }];
  }
}

- (NSString *)p_formattedValueString:(NSNumberFormatter *)formatter value:(NSNumber *)value
{
  NSString *formattedValueString = [formatter stringFromNumber:value];
  return formattedValueString ? formattedValueString : @"---";
}

- (void)p_updateLabel:(NSDictionary *)values
{
#if defined(OLD_SENSORTAG_COMPATIBLE_JSON)
  NSNumber *accX = values[@"accelerationX"];
  NSNumber *accY = values[@"accelerationY"];
  NSNumber *accZ = values[@"accelerationZ"];
  NSNumber *gyroX = values[@"angularVelocityX"];
  NSNumber *gyroY = values[@"angularVelocityY"];
  NSNumber *gyroZ = values[@"angularVelocityZ"];
  NSNumber *magX = values[@"magneticFieldX"];
  NSNumber *magY = values[@"magneticFieldY"];
  NSNumber *magZ = values[@"magneticFieldZ"];
  NSNumber *baroPress = values[@"pressure"];
  NSNumber *ambTemp = values[@"ambientTemperature"];
  NSNumber *IRTemp = values[@"objctTemperature"];
  NSNumber *humidity = values[@"relativeHumidity"];
#else
  NSNumber *accX = values[@"accX"];
  NSNumber *accY = values[@"accY"];
  NSNumber *accZ = values[@"accZ"];
  NSNumber *gyroX = values[@"gyroX"];
  NSNumber *gyroY = values[@"gyroY"];
  NSNumber *gyroZ = values[@"gyroZ"];
  NSNumber *magX = values[@"magX"];
  NSNumber *magY = values[@"magY"];
  NSNumber *magZ = values[@"magZ"];
  NSNumber *baroPress = values[@"baroPres"];
  NSNumber *ambTemp = values[@"AmbTemp"];
  NSNumber *IRTemp = values[@"IRTemp"];
  NSNumber *humidity = values[@"humidity"];
#endif
  NSNumber *optical = values[@"optical"];
  NSNumber *key1 = values[@"key1"];
  NSNumber *key2 = values[@"key2"];

  NSNumberFormatter* formatter = [[NSNumberFormatter alloc] init];
  formatter.numberStyle = NSNumberFormatterDecimalStyle;

  formatter.minimumFractionDigits = 0;
  formatter.maximumFractionDigits = 0;
  self.pressureLabel.text = [NSString stringWithFormat:@"%@hPa", [self p_formattedValueString:formatter value:baroPress]];
  self.userButtonLabel.text = [NSString stringWithFormat:@"%@", [self p_formattedValueString:formatter value:key1]];
  self.powerButtonLabel.text = [NSString stringWithFormat:@"%@", [self p_formattedValueString:formatter value:key2]];
  
  formatter.minimumFractionDigits = 1;
  formatter.maximumFractionDigits = 1;
  self.ambientTemperatureLabel.text = [NSString stringWithFormat:@"%@°C", [self p_formattedValueString:formatter value:ambTemp]];
  self.objctTemperatureLabel.text = [NSString stringWithFormat:@"%@°C", [self p_formattedValueString:formatter value:IRTemp]];
  self.relativeHumidityLabel.text = [NSString stringWithFormat:@"%@%%rH", [self p_formattedValueString:formatter value:humidity]];
  self.luminousIntensityLabel.text = [NSString stringWithFormat:@"%@lux", [self p_formattedValueString:formatter value:optical]];
  
  formatter.minimumFractionDigits = 2;
  formatter.maximumFractionDigits = 2;
  self.accelerationXLabel.text = [NSString stringWithFormat:@"%@G", [self p_formattedValueString:formatter value:accX]];
  self.accelerationYLabel.text = [NSString stringWithFormat:@"%@G", [self p_formattedValueString:formatter value:accY]];
  self.accelerationZLabel.text = [NSString stringWithFormat:@"%@G", [self p_formattedValueString:formatter value:accZ]];
  self.angularVelocityXLabel.text = [NSString stringWithFormat:@"%@°/s", [self p_formattedValueString:formatter value:gyroX]];
  self.angularVelocityYLabel.text = [NSString stringWithFormat:@"%@°/s", [self p_formattedValueString:formatter value:gyroY]];
  self.angularVelocityZLabel.text = [NSString stringWithFormat:@"%@°/s", [self p_formattedValueString:formatter value:gyroZ]];
  self.magneticFieldXLabel.text =  [NSString stringWithFormat:@"%@μT", [self p_formattedValueString:formatter value:magX]];
  self.magneticFieldYLabel.text =  [NSString stringWithFormat:@"%@μT", [self p_formattedValueString:formatter value:magY]];
  self.magneticFieldZLabel.text =  [NSString stringWithFormat:@"%@μT", [self p_formattedValueString:formatter value:magZ]];
}

- (void)p_showSelector
{
  [self performSegueWithIdentifier:@"SensorTagSelector" sender:self];
}


#pragma mark - <SensorTagSelectorViewControllerDelegate>

- (void)controller:(SensorTagSelectorViewController *)controller didSelectSensorTag:(STSensorTag *)sensorTag
{
  [controller dismissViewControllerAnimated:YES completion:NULL];
  
  NSLog(@"%@", sensorTag);
}

- (void)controllerDidTapCloseButton:(SensorTagSelectorViewController *)controller
{
  [controller dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - <IASKSettingsDelegate>

- (void)settingsViewControllerDidEnd:(IASKAppSettingsViewController *)sender
{
  [sender dismissViewControllerAnimated:YES completion:nil];

  // 設定を反映

  if (!_appSettings.isTransmissionEnabled) {
    self.title = nil;
    [_idcfSubscriber unsubscribe];
  }
  else {
    [_idcfSubscriber subscribe];
  }

  _transmitter.host = _appSettings.host;
  _transmitter.triggerUUID = _appSettings.destinationTrigger.uuid;
  _transmitter.triggerToken = _appSettings.destinationTrigger.token;
  _transmitter.devices = _appSettings.forwardingActionUUIDs;

  // NOTE: isTransmissionEnabledやtransmissionIntervalはUserDefaultsを直接読んで返しているので設定変更したときには反映されている
}

- (void)settingsViewController:(IASKAppSettingsViewController *)sender buttonTappedForSpecifier:(IASKSpecifier *)specifier
{
  if ([specifier.key isEqualToString:@"QRCodeReaderLaunchAction"]) {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController *navigationController = [storyboard instantiateViewControllerWithIdentifier:@"QRCodeReader"];
    QRCodeReaderViewController *vc = (QRCodeReaderViewController *)navigationController.topViewController;
    NSParameterAssert([vc isKindOfClass:[QRCodeReaderViewController class]]);
    vc.delegate = self;
    
    [sender presentViewController:navigationController animated:YES completion:nil];
    
  }
  
}

#pragma mark - <QRCodeReaderViewControllerDelegate>

- (void)qrCodeReaderDidTapCloseButton:(QRCodeReaderViewController *)controller
{
  [controller dismissViewControllerAnimated:YES completion:NULL];
}

- (void)qrCodeReaderDidTapImportButton:(QRCodeReaderViewController *)controller
{
  NSLog(@"%@", controller.detectString);
  [AppSettings importMeshbluDevicesFromJSONString:controller.detectString];
  [controller dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - Actions

- (IBAction)settingsButtonDidPush:(id)sender
{
  UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:self.appSettingsViewController];
  self.appSettingsViewController.showDoneButton = YES;
  
  [self presentViewController:navigationController animated:YES completion:nil];
}
@end
