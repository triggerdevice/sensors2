//
//  STDeviceManager.h
//  Sensors
//
//  Created by calmscape on 2015/09/18.
//  Copyright (c) 2015年 TriggerDevice. All rights reserved.
//

#import <Foundation/Foundation.h>

@class STSensorTag;

typedef void (^sensorTagFoundBlock)(STSensorTag *sensorTag);
typedef void (^sensorTagConnectionSuccessBlock)(STSensorTag *sensorTag);
typedef void (^sensorTagConnectionFailureBlock)(NSError *error);

@interface STDeviceManager : NSObject

@property (readonly, nonatomic, strong) STSensorTag *sensorTag;

/*
 * DeviceManagerの準備ができた。
 * スキャン等の操作は本プロパティがYES状態でないと動作しない。
 */
@property (readonly, nonatomic, assign) BOOL isReady;

/// 接続中か否か
@property (readonly, nonatomic, assign) BOOL isConnected;

/// センサータグのスキャンを開始する
- (void)startScanningWithFoundBlock:(sensorTagFoundBlock)foundBlock;

/// センサータグのスキャンを終了する
- (void)stopScanning;

- (void)connectSensorTagWithSuccessBlock:(sensorTagConnectionSuccessBlock)successBlock failureBlock:(sensorTagConnectionFailureBlock)failureBlock;
- (void)cancelSensorTagConnection;

@end
