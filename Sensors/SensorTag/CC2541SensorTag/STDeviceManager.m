//
//  STDeviceManager.m
//  Sensors
//
//  Created by calmscape on 2015/09/18.
//  Copyright (c) 2015年 TriggerDevice. All rights reserved.
//

@import CoreBluetooth;
#import "STSensorTag.h"
#import "STDeviceManager.h"


@interface STDeviceManager () <CBCentralManagerDelegate>
@property (nonatomic, strong) CBCentralManager *centralManager;
@property (readwrite, nonatomic, assign) BOOL isReady;
@property (readwrite, nonatomic, assign) BOOL isConnected;

@property (readwrite, nonatomic, strong) STSensorTag *sensorTag;
@property (nonatomic, copy) sensorTagFoundBlock peripheralFoundBlock;
@property (nonatomic, copy) sensorTagConnectionSuccessBlock peripheralConnectionSuccessBlock;
@property (nonatomic, copy) sensorTagConnectionFailureBlock peripheralConnectionFailureBlock;
@end

@implementation STDeviceManager

- (instancetype)init
{
  self = [super init];
  if (self) {
    _isReady = NO;
    _isConnected = NO;
    NSDictionary *options = @{ CBCentralManagerOptionShowPowerAlertKey: @YES, };
    _centralManager = [[CBCentralManager alloc] initWithDelegate:self queue:nil options:options];
  }
  return self;
}

- (void)startScanningWithFoundBlock:(sensorTagFoundBlock)foundBlock;
{
  NSLog(@"%s", __PRETTY_FUNCTION__);
  _peripheralFoundBlock = foundBlock;
  
  NSDictionary *options = @{ CBCentralManagerScanOptionAllowDuplicatesKey: @YES, };
  [_centralManager scanForPeripheralsWithServices:nil
                                          options:options];
}

- (void)stopScanning
{
  [_centralManager stopScan];
}

- (void)connectSensorTagWithSuccessBlock:(sensorTagConnectionSuccessBlock)successBlock failureBlock:(sensorTagConnectionFailureBlock)failureBlock
{
  if (_sensorTag.peripheral.state != CBPeripheralStateDisconnected) {
    return;
  }
  _peripheralConnectionSuccessBlock = successBlock;
  _peripheralConnectionFailureBlock = failureBlock;

  [_centralManager connectPeripheral:_sensorTag.peripheral options:nil];
}

- (void)cancelSensorTagConnection
{
  if (_sensorTag.peripheral.state == CBPeripheralStateDisconnected) {
    return;
  }
 
  [_centralManager cancelPeripheralConnection:_sensorTag.peripheral];
}

#pragma mark - <CBCentralManagerDelegate>

- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral
{
  NSLog(@"%s", __PRETTY_FUNCTION__);

  self.isConnected = YES;

  // つながったら全サービスを探索する。見つかった場合の通知はPeripheral側のDelegateに飛ぶ。
  [peripheral discoverServices:nil];
  
  if (_peripheralConnectionSuccessBlock) {
    _peripheralConnectionSuccessBlock(_sensorTag);
    _peripheralConnectionSuccessBlock = nil;
    _peripheralConnectionFailureBlock = nil;
  }
}

- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
  NSLog(@"%s", __PRETTY_FUNCTION__);
  self.isConnected = NO;
}

- (void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
  NSLog(@"%s, %@", __PRETTY_FUNCTION__, [error localizedDescription]);
  
  if (_peripheralConnectionFailureBlock) {
    _peripheralConnectionFailureBlock(error);
    _peripheralConnectionSuccessBlock = nil;
    _peripheralConnectionFailureBlock = nil;
  }
}

- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI
{
  BOOL isProximityDevice = (-45 < RSSI.integerValue) && (RSSI.integerValue < 0);
  if (!isProximityDevice) {
    return;
  }
  
  NSString *localName = advertisementData[CBAdvertisementDataLocalNameKey];
  BOOL isFoundSensorTag = [STSensorTag classWithName:localName] != nil;

  if (isFoundSensorTag) {
    [central stopScan];

    NSLog(@"SensorTag found! [%@] %@, %@", RSSI, peripheral.name, advertisementData[CBAdvertisementDataLocalNameKey]);
    if (_peripheralFoundBlock) {
      _sensorTag = [[[STSensorTag classWithName:localName] alloc] initWithPeripheral:peripheral];
      _peripheralFoundBlock(_sensorTag);
      _peripheralFoundBlock = nil;
    }
  }
}

- (void)centralManager:(CBCentralManager *)central didRetrieveConnectedPeripherals:(NSArray *)peripherals
{
}

- (void)centralManager:(CBCentralManager *)central didRetrievePeripherals:(NSArray *)peripherals
{
}

// required
- (void)centralManagerDidUpdateState:(CBCentralManager *)central
{
  __unused NSArray *stateString = @[@"CBCentralManagerStateUnknown",
                                    @"CBCentralManagerStateResetting",
                                    @"CBCentralManagerStateUnsupported",
                                    @"CBCentralManagerStateUnauthorized",
                                    @"CBCentralManagerStatePoweredOff",
                                    @"CBCentralManagerStatePoweredOn",];
  NSLog(@"%s, state=%@", __PRETTY_FUNCTION__, stateString[central.state]);

  self.isReady = (central.state == CBCentralManagerStatePoweredOn) ? YES : NO;
}

- (void)centralManager:(CBCentralManager *)central willRestoreState:(NSDictionary *)dict
{
}


@end
