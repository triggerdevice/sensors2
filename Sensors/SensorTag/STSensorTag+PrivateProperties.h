//
//  STSensorTag+PrivateProperties.h
//  Sensors2
//
//  Created by calmscape on 2015/10/09.
//  Copyright © 2015年 TriggerDevice. All rights reserved.
//

#import "STSensorTag.h"

@class CBService;

@interface STSensorTag (PrivateProperties)
@property (nonatomic, strong) NSMutableArray *sensors;

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error;
@end

