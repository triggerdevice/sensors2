//
//  STSensor+Factory.m
//  Sensors
//
//  Created by Masahiro Murase on 2015/09/20.
//  Copyright (c) 2015年 TriggerDevice. All rights reserved.
//

#import "STSensorConfiguration.h"
#import "STSensor+Factory.h"

@implementation STSensor (Factory)

+ (STSensor *)sensorWithService:(CBService *)service configuration:(STSensorConfiguration *)configuration
{
  NSDictionary *sensorConfiguraiton = [configuration dictionaryFromServiceUUID:service.UUID.UUIDString];
  if (!sensorConfiguraiton) {
    return nil;
  }

  NSDictionary * const classNames = @{
                                      kIRTemperatureSensorNameKey:      @"STTemperatureSensor",
                                      kAccelerometerSensorNameKey:      @"STAccelerometerSensor",
                                      kMovementSensorNameKey:           @"STMovementSensor",
                                      kHumiditySensorNameKey:           @"STHumiditySensor",
                                      kMagnetometerSensorNameKey:       @"STMagnetometerSensor",
                                      kBarometricPressureSensorNameKey: @"STBarometricPressureSensor",
                                      kGyroscopeSensorNameKey:          @"STGyroscopeSensor",
                                      kOpticalSensorNameKey:            @"STOpticalSensor",
                                      kIOServiceNameKey:                @"STIOService",
                                      kSimpleKeyServiceNameKey:         @"STSimpleKeyService",
                                      };

  NSString *className = classNames[sensorConfiguraiton[kSensorNameKey]];
  
  return [[NSClassFromString(className) alloc] initWithService:service configuration:configuration];
}

@end
