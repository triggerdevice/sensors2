//
//  STSensorConfiguration.h
//  Sensors
//
//  Created by Masahiro Murase on 2015/09/20.
//  Copyright (c) 2015年 TriggerDevice. All rights reserved.
//

#import <Foundation/Foundation.h>

// 辞書から設定情報を取得するためのキー
extern NSString * const kSensorNameKey;
extern NSString * const kServiceUUIDKey;
extern NSString * const kDataCharacteristicUUIDKey;
extern NSString * const kConfigurationCharacteristicUUIDKey;
extern NSString * const kCalibrationCharacteristicUUIDKey;
extern NSString * const kPeriodCharacteristicUUIDKey;
extern NSString * const kPeriodKey;

extern NSString * const kIRTemperatureSensorNameKey;
extern NSString * const kAccelerometerSensorNameKey;
extern NSString * const kMovementSensorNameKey;
extern NSString * const kHumiditySensorNameKey;
extern NSString * const kMagnetometerSensorNameKey;
extern NSString * const kBarometricPressureSensorNameKey;
extern NSString * const kGyroscopeSensorNameKey;
extern NSString * const kOpticalSensorNameKey;
extern NSString * const kIOServiceNameKey;
extern NSString * const kSimpleKeyServiceNameKey;

@interface STSensorConfiguration : NSObject

/// センサー更新間隔のデフォルト値
+ (float)defaultPeriod;

/// デフォルトの設定ファイル(SensorSettings.json)を使用してインスタンス化する
+ (instancetype)defaultConfiguration;

/// 設定ファイルのURLを指定して初期化する
- (instancetype)initWithContentsOfURL:(NSURL *)aURL;

/// センサーのサービスUUIDをもとにそのセンサーの設定を辞書形式で取得する
- (NSDictionary *)dictionaryFromServiceUUID:(NSString *)aUUIDString;


- (instancetype)init __attribute__((unavailable("unavailable initializer")));

@end
