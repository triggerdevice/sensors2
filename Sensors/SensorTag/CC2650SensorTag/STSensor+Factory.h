//
//  STSensor+Factory.h
//  Sensors
//
//  Created by Masahiro Murase on 2015/09/20.
//  Copyright (c) 2015年 TriggerDevice. All rights reserved.
//

#import "STSensor.h"

@interface STSensor (Factory)

/// サービスに対応するセンサーを生成して返す
+ (STSensor *)sensorWithService:(CBService *)service configuration:(STSensorConfiguration *)configuration;

@end
