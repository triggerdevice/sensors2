//
//  STBarometricPressureSensor.h
//  Sensors
//
//  Created by Masahiro Murase on 2015/12/04.
//  Copyright (c) 2015年 TriggerDevice. All rights reserved.
//

#import "STSensor.h"

/*
 * JSON
 *
 * Key-Value
 * baroPres: 浮動小数点数 // 大気圧 [hPa]
 * baroTemp: 浮動小数点数   // 温度 [℃]
 *
 */

@interface STBarometricPressureSensor : STSensor

/// 大気圧 [hPa]
@property (readonly, nonatomic, strong) NSNumber *baroPres;

/// 温度 [℃]
@property (readonly, nonatomic, strong) NSNumber *baro_t;

@end
