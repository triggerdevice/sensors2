//
//  STMovementSensor.m
//  Sensors
//
//  Created by Masahiro Murase on 2015/12/03.
//  Copyright (c) 2015年 TriggerDevice. All rights reserved.
//

#import "STMovementSensor.h"

@implementation STMovementSensor

- (instancetype)initWithService:(CBService *)service configuration:(STSensorConfiguration *)configuration;
{
  self = [super initWithService:service configuration:configuration];
  if (self) {
    _accRange = kAccRange2G;
  }
  return self;
}

- (NSDictionary *)snapshot
{
  return @{
#if defined(OLD_SENSORTAG_COMPATIBLE_JSON)
           @"angularVelocityX": self.gyroX,
           @"angularVelocityY": self.gyroY,
           @"angularVelocityZ": self.gyroZ,
           @"accelerationX": self.accX,
           @"accelerationY": self.accY,
           @"accelerationZ": self.accZ,
           @"magneticFieldX": self.magX,
           @"magneticFieldY": self.magY,
           @"magneticFieldZ": self.magZ,
#else
           @"gyroX": self.gyroX,
           @"gyroY": self.gyroY,
           @"gyroZ": self.gyroZ,
           @"accX": self.accX,
           @"accY": self.accY,
           @"accZ": self.accZ,
           @"magX": self.magX,
           @"magY": self.magY,
           @"magZ": self.magZ,
#endif
           };
}

- (void)setAccRange:(AccelerometerRange)accRange
{
  if (_accRange == accRange) {
    return;
  }
  _accRange = accRange;
  [self enable];
}

- (void)enable
{
  // enable sensor
  uint16_t data = 0x0000;
  data |= 0x0007; // Enable Gyroscope
  data |= 0x0038; // Enable Accelerometer
  data |= 0x0040; // Enable Magnetometer
  data |= (_accRange << 8); // Accelerometer range
  NSLog(@"Movement configuration: 0x%04x", data);
  NSData *writeValue = [NSData dataWithBytes:&data length:sizeof(data)];
  [self.service.peripheral writeValue:writeValue forCharacteristic:self.configurationCharacteristic type:CBCharacteristicWriteWithResponse];
}

- (void)disable
{
  // disable sensor
  uint16_t data = 0x00;
  NSData *writeValue = [NSData dataWithBytes:&data length:sizeof(data)];
  [self.service.peripheral writeValue:writeValue forCharacteristic:self.configurationCharacteristic type:CBCharacteristicWriteWithResponse];
}

- (NSNumber *)gyroX
{
  NSData *rawData = self.dataCharacteristic.value;
  if (!rawData) {
    return @(-0.0);
  }

  int16_t value = 0;
  [rawData getBytes:&value range:NSMakeRange(0, sizeof(value))];
  return @(sensorMpu9250GyroConvert(value));
}

- (NSNumber *)gyroY
{
  NSData *rawData = self.dataCharacteristic.value;
  if (!rawData) {
    return @(-0.0);
  }

  int16_t value = 0;
  [rawData getBytes:&value range:NSMakeRange(2, sizeof(value))];
  return @(sensorMpu9250GyroConvert(value));
}

- (NSNumber *)gyroZ
{
  NSData *rawData = self.dataCharacteristic.value;
  if (!rawData) {
    return @(-0.0);
  }

  int16_t value = 0;
  [rawData getBytes:&value range:NSMakeRange(4, sizeof(value))];
  return @(sensorMpu9250GyroConvert(value));
}

- (NSNumber *)accX
{
  NSData *rawData = self.dataCharacteristic.value;
  if (!rawData) {
    return @(-0.0);
  }
  
  int16_t value = 0;
  [rawData getBytes:&value range:NSMakeRange(6, sizeof(value))];
  return @([self p_sensorMpu9250AccConvert:value]);
}

- (NSNumber *)accY
{
  NSData *rawData = self.dataCharacteristic.value;
  if (!rawData) {
    return @(-0.0);
  }
  
  int16_t value = 0;
  [rawData getBytes:&value range:NSMakeRange(8, sizeof(value))];
  return @([self p_sensorMpu9250AccConvert:value]);
}

- (NSNumber *)accZ
{
  NSData *rawData = self.dataCharacteristic.value;
  if (!rawData) {
    return @(-0.0);
  }
  
  int16_t value = 0;
  [rawData getBytes:&value range:NSMakeRange(10, sizeof(value))];
  return @([self p_sensorMpu9250AccConvert:value]);
}

- (NSNumber *)magX
{
  NSData *rawData = self.dataCharacteristic.value;
  if (!rawData) {
    return @(-0.0);
  }
  
  int16_t value = 0;
  [rawData getBytes:&value range:NSMakeRange(12, sizeof(value))];
  return @(sensorMpu9250MagConvert(value));
}

- (NSNumber *)magY
{
  NSData *rawData = self.dataCharacteristic.value;
  if (!rawData) {
    return @(-0.0);
  }
  
  int16_t value = 0;
  [rawData getBytes:&value range:NSMakeRange(14, sizeof(value))];
  return @(sensorMpu9250MagConvert(value));
}

- (NSNumber *)magZ
{
  NSData *rawData = self.dataCharacteristic.value;
  if (!rawData) {
    return @(-0.0);
  }
  
  int16_t value = 0;
  [rawData getBytes:&value range:NSMakeRange(16, sizeof(value))];
  return @(sensorMpu9250MagConvert(value));
}

#pragma mark - Conversion algorithm from the raw data to a sensor value

float sensorMpu9250GyroConvert(int16_t rawData)
{
  //-- calculate rotation, unit deg/s, range -250, +250
  return (rawData * 1.0) / (65536.0 / 500.0);
}

- (float)p_sensorMpu9250AccConvert:(int16_t)rawData
{
  float v;

  switch (_accRange)
  {
    case kAccRange2G:
      //-- calculate acceleration, unit G, range -2, +2
      v = (rawData * 1.0) / (32768.0/2);
      break;
      
    case kAccRange4G:
      //-- calculate acceleration, unit G, range -4, +4
      v = (rawData * 1.0) / (32768.0/4);
      break;
      
    case kAccRange8G:
      //-- calculate acceleration, unit G, range -8, +8
      v = (rawData * 1.0) / (32768.0/8);
      break;
      
    case kAccRange16G:
      //-- calculate acceleration, unit G, range -16, +16
      v = (rawData * 1.0) / (32768.0/16);
      break;
  }

  return v;
}

float sensorMpu9250MagConvert(int16_t rawData)
{
  //-- calculate magnetism, unit uT, range +-4900

#if 1
  // Refer to https://github.com/sandeepmistry/node-sensortag/blob/master/lib/cc2650.js
  return rawData * 4912.0 / 32760.0;
#else
  // 値がおかしい...
  return 1.0 * rawData;
#endif
}


@end


