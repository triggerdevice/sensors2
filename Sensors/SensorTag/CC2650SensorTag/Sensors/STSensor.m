//
//  STSensor.m
//  Sensors
//
//  Created by Masahiro Murase on 2015/09/19.
//  Copyright (c) 2015年 TriggerDevice. All rights reserved.
//

#import "STSensorConfiguration.h"
#import "STSensor.h"

@interface STSensor ()
@property (readwrite, nonatomic, weak) CBService *service;
@property (readwrite, nonatomic, weak) CBCharacteristic *dataCharacteristic;
@property (readwrite, nonatomic, weak) CBCharacteristic *configurationCharacteristic;
@property (readwrite, nonatomic, weak) CBCharacteristic *periodCharacteristic;

@property (nonatomic, copy) NSString *serviceUUIDString;
@property (nonatomic, copy) NSString *dataCharacteristicUUIDString;
@property (nonatomic, copy) NSString *configurationCharacteristicUUIDString;
@property (nonatomic, copy) NSString *periodCharacteristicUUIDString;

@end

@implementation STSensor

- (instancetype)initWithService:(CBService *)service configuration:(STSensorConfiguration *)configuration
{
  self = [super init];
  if (self) {
    _service = service;

    NSDictionary *config = [configuration dictionaryFromServiceUUID:service.UUID.UUIDString];
    _serviceUUIDString = config[kServiceUUIDKey];
    _dataCharacteristicUUIDString = config[kDataCharacteristicUUIDKey];
    _configurationCharacteristicUUIDString = config[kConfigurationCharacteristicUUIDKey];
    _periodCharacteristicUUIDString = config[kPeriodCharacteristicUUIDKey];

    [self p_configureCharacteristics];
  }
  return self;
}

- (BOOL)hasCharacteristicWithUUID:(NSString *)characteristicUUIDString
{
  for (CBCharacteristic *characteristic in _service.characteristics) {
    if ([characteristic.UUID.UUIDString isEqualToString:characteristicUUIDString]) {
      return YES;
    }
  }
  return NO;
}

- (NSDictionary *)snapshot
{
  return nil;
}

- (NSString *)description
{
  return [[self snapshot] description];
}

- (void)calibrate
{
}

- (void)enable
{
  if (!_configurationCharacteristic) {
    return;
  }

  // enable sensor
  uint8_t data = 0x01;
  NSData *writeValue = [NSData dataWithBytes:&data length:sizeof(data)];
  [_service.peripheral writeValue:writeValue forCharacteristic:_configurationCharacteristic type:CBCharacteristicWriteWithResponse];
}

- (void)disable
{
  if (!_configurationCharacteristic) {
    return;
  }

  // disable sensor
  uint8_t data = 0x00;
  NSData *writeValue = [NSData dataWithBytes:&data length:sizeof(data)];
  [_service.peripheral writeValue:writeValue forCharacteristic:_configurationCharacteristic type:CBCharacteristicWriteWithResponse];
}

#pragma mark - accessor

- (NSArray *)characteristics
{
  return _service.characteristics;
}

- (void)setNotificationEnabled:(BOOL)notificationEnabled
{
  if (_notificationEnabled == notificationEnabled) {
    return;
  }
  _notificationEnabled = notificationEnabled;
  
  if (_notificationEnabled) {
    [self p_enableNotificationFromSensor];
  }
  else {
    [self p_disableNotificationFromSensor];
  }
}

- (void)setPeriod:(float)seconds
{
  if (!_periodCharacteristic) {
    return;
  }
  
  // Period characteristic
  float msPeriod = seconds * 1000;  //ms単位にしておく

  uint8_t period = (uint8_t)(msPeriod / 10);  // 10ms単位で指定する必要がある
  NSData *writeValue = [NSData dataWithBytes:&period length:sizeof(period)];
  [_service.peripheral writeValue:writeValue forCharacteristic:_periodCharacteristic type:CBCharacteristicWriteWithResponse];
}

- (void)sensorDidUpdateValueForCharacteristic:(CBCharacteristic *)characteristic
{
}

- (void)sensorDidFinishConfiguration
{
}

#pragma mark -

- (void)p_enableNotificationFromSensor
{
  // enable notification
  [_service.peripheral setNotifyValue:YES forCharacteristic:_dataCharacteristic];
}

- (void)p_disableNotificationFromSensor
{
  // disable notification
  [_service.peripheral setNotifyValue:NO forCharacteristic:_dataCharacteristic];
}

- (void)p_configureCharacteristics
{
  for (CBCharacteristic *characteristic in _service.characteristics) {
    if ([_dataCharacteristicUUIDString isEqualToString:characteristic.UUID.UUIDString]) {
      _dataCharacteristic = characteristic;
    }
    else if ([_configurationCharacteristicUUIDString isEqualToString:characteristic.UUID.UUIDString]) {
      _configurationCharacteristic = characteristic;
    }
    else if ([_periodCharacteristicUUIDString isEqualToString:characteristic.UUID.UUIDString]) {
      _periodCharacteristic = characteristic;
    }
  }
}


@end
