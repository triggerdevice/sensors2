//
//  STIOService.m
//  Sensors2
//
//  Created by Masahiro Murase on 2015/12/05.
//  Copyright © 2015年 TriggerDevice. All rights reserved.
//

#import "STIOService.h"

/// Blinkパターンの定義'o'はON, '.'はoffを表す。8文字で定義するとなら1秒間に8回on/offを切り替えることができる。
static NSString * const kRedLEDBlinkPattern = @"oooo....";  // 0.5秒ON,0.5秒OFF
static NSString * const kGreenLEDBlinkPattern = @"oooo....";
static NSString * const kBuzzerBlinkPattern = @"oooo....";

typedef NS_OPTIONS(uint8_t, STIOServiceActivatingIO) {
  STIOServiceActivatingIORedLED    = 1 << 0,
  STIOServiceActivatingIOGreenLED  = 1 << 1,
  STIOServiceActivatingIOBuzzer    = 1 << 2,
};

typedef NS_ENUM(uint8_t, ConfigurationMode) {
  ConfigurationModeLocal  = 0,
  ConfigurationModeRemote = 1,
  ConfigurationModeTest   = 2,
};

@interface STIOService ()
@property (nonatomic, assign) uint8_t activatingIOData;

@property (nonatomic, strong) NSTimer *ledBlinkTimer;
@property (nonatomic, assign) NSUInteger blinkCounter;

@property (nonatomic, assign) BOOL isTurnOnRedLED;
@property (nonatomic, assign) BOOL isTurnOnGreenLED;
@property (nonatomic, assign) BOOL isTurnOnBuzzer;
@end


@implementation STIOService

- (instancetype)initWithService:(CBService *)service configuration:(STSensorConfiguration *)configuration
{
  self = [super initWithService:service configuration:configuration];
  if (self) {
    _activatingIOData = 0x00;

    _greenLEDState = ST2IOServiceIOStateOff;
    _redLEDState = ST2IOServiceIOStateOff;
    _buzzerState = ST2IOServiceIOStateOff;
    
    _ledBlinkTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 / kRedLEDBlinkPattern.length
                                                      target:self
                                                    selector:@selector(p_blinkTImerDidFire:)
                                                    userInfo:nil
                                                     repeats:YES];
    _blinkCounter = 0;
  }
  return self;
}

- (void)enable
{
  // Local mode時に値が入っているっぽいのでゼロ初期化しておく
  uint8_t data = 0x00;
  [self.service.peripheral writeValue:[NSData dataWithBytes:&data length:sizeof(data)]
                    forCharacteristic:self.dataCharacteristic
                                 type:CBCharacteristicWriteWithResponse];
  _activatingIOData = data;

  [self p_setRemoteMode];
}

- (void)disable
{
  [self p_setLocalMode];
}

- (void)setNotificationEnabled:(BOOL)notificationEnabled
{
  return;
}

#if 0
- (void)turnOn:(STIOServiceActivatingIO)type
{
  uint8_t value = 0;
  value |= type;

  NSData *writeData = [NSData dataWithBytes:&value length:sizeof(value)];
  [self.service.peripheral writeValue:writeData forCharacteristic:self.dataCharacteristic type:CBCharacteristicWriteWithResponse];
}

-(void)turnOff:(STIOServiceActivatingIO)type
{
  uint8_t value = 0;
  value &= ~type;
  
  NSData *writeData = [NSData dataWithBytes:&value length:sizeof(value)];
  [self.service.peripheral writeValue:writeData forCharacteristic:self.dataCharacteristic type:CBCharacteristicWriteWithResponse];
}
#endif

- (void)applyPayloadString:(NSString *)payload;
{
  NSDictionary * const ioState = @{
                                   @"greenled-off": @(ST2IOServiceIOStateOff),
                                   @"greenled-on": @(ST2IOServiceIOStateOn),
                                   @"greenled-blink": @(ST2IOServiceIOStateBlink),
                                   @"redled-off": @(ST2IOServiceIOStateOff),
                                   @"redled-on": @(ST2IOServiceIOStateOn),
                                   @"redled-blink": @(ST2IOServiceIOStateBlink),
                                   @"buzzer-off": @(ST2IOServiceIOStateOff),
                                   @"buzzer-on": @(ST2IOServiceIOStateOn),
                                   @"buzzer-blink": @(ST2IOServiceIOStateBlink),
                                   };
  
  
  __weak typeof(self) weakSelf = self;
  NSArray *components = [payload componentsSeparatedByString:@"\n"];
  
  [components enumerateObjectsUsingBlock:^(NSString *component, NSUInteger idx, BOOL *stop) {
    NSLog(@"%@", component);
    dispatch_async(dispatch_get_main_queue(), ^{
      if ([component hasPrefix:@"greenled"]) {
        weakSelf.greenLEDState = [ioState[component] integerValue];
      }
      else if ([component hasPrefix:@"redled"]) {
        weakSelf.redLEDState = [ioState[component] integerValue];
      }
      else if ([component hasPrefix:@"buzzer"]) {
        weakSelf.buzzerState = [ioState[component] integerValue];
      }
    });
  }];
}

#pragma mark - Accessor

- (void)setRedLEDState:(ST2IOServiceIOState)redLEDState
{
  if (_redLEDState == redLEDState) {
    return;
  }
  _redLEDState = redLEDState;
}

- (void)setGreenLEDState:(ST2IOServiceIOState)greenLEDState
{
  if (_greenLEDState == greenLEDState) {
    return;
  }
  _greenLEDState = greenLEDState;
}

- (void)setBuzzerState:(ST2IOServiceIOState)buzzerState
{
  if (_buzzerState == buzzerState) {
    return;
  }
  _buzzerState = buzzerState;
  
  if (_buzzerState == ST2IOServiceIOStateOn) {
    [self p_setActivatingIOBit:STIOServiceActivatingIOBuzzer];
  }
  else {
    [self p_clearActivatingIOBit:STIOServiceActivatingIOBuzzer];
  }
}

- (void)setIsTurnOnRedLED:(BOOL)isTurnOnRedLED
{
  if (_isTurnOnRedLED == isTurnOnRedLED) {
    return;
  }
  _isTurnOnRedLED = isTurnOnRedLED;
  
  if (_isTurnOnRedLED) {
    [self p_setActivatingIOBit:STIOServiceActivatingIORedLED];
  }
  else {
    [self p_clearActivatingIOBit:STIOServiceActivatingIORedLED];
  }
}

- (void)setIsTurnOnGreenLED:(BOOL)isTurnOnGreenLED
{
  if (_isTurnOnGreenLED == isTurnOnGreenLED) {
    return;
  }
  _isTurnOnGreenLED = isTurnOnGreenLED;
  
  if (_isTurnOnGreenLED) {
    [self p_setActivatingIOBit:STIOServiceActivatingIOGreenLED];
  }
  else {
    [self p_clearActivatingIOBit:STIOServiceActivatingIOGreenLED];
  }
}

- (void)setIsTurnOnBuzzer:(BOOL)isTurnOnBuzzer
{
  if (_isTurnOnBuzzer == isTurnOnBuzzer) {
    return;
  }
  _isTurnOnBuzzer = isTurnOnBuzzer;
  
  if (_isTurnOnBuzzer) {
    [self p_setActivatingIOBit:STIOServiceActivatingIOBuzzer];
  }
  else {
    [self p_clearActivatingIOBit:STIOServiceActivatingIOBuzzer];
  }
}

#pragma mark - Private instance methods

- (void)p_blinkTImerDidFire:(NSTimer *) timer
{
  _blinkCounter++;
  
  BOOL shouldTurnOnRedLED = [self p_shouldTurnOnWithState:_redLEDState pattern:kRedLEDBlinkPattern];
  self.isTurnOnRedLED = shouldTurnOnRedLED;
  
  BOOL shouldTurnOnGreenLED = [self p_shouldTurnOnWithState:_greenLEDState pattern:kGreenLEDBlinkPattern];
  self.isTurnOnGreenLED = shouldTurnOnGreenLED;

  BOOL shouldTurnOnBuzzer = [self p_shouldTurnOnWithState:_buzzerState pattern:kBuzzerBlinkPattern];
  self.isTurnOnBuzzer = shouldTurnOnBuzzer;
}

- (BOOL)p_shouldTurnOnWithState:(ST2IOServiceIOState)state pattern:(NSString *)pattern
{
  if (state == ST2IOServiceIOStateOn) {
    return YES;
  }
  else if (state == ST2IOServiceIOStateOff) {
    return NO;
  }
  
  // Blink
  unichar c = [pattern characterAtIndex:(_blinkCounter % 8)];
  if (c == 'o') {
    return YES;
  }
  return NO;
}

- (void)p_setActivatingIOBit:(uint8_t)bit
{
  uint8_t activatingIOData = _activatingIOData;
  activatingIOData |= bit;
  
  [self p_writeActivatingIOValue:activatingIOData];
}

- (void)p_clearActivatingIOBit:(uint8_t)bit
{
  uint8_t activatingIOData = _activatingIOData;
  activatingIOData &= ~bit;
  
  [self p_writeActivatingIOValue:activatingIOData];
}

- (void)p_writeActivatingIOValue:(uint8_t)value
{
  NSData *writeData = [NSData dataWithBytes:&value length:sizeof(value)];
  [self.service.peripheral writeValue:writeData
                    forCharacteristic:self.dataCharacteristic
                                 type:CBCharacteristicWriteWithResponse];
  _activatingIOData = value;
}

- (void)p_writeConfigurationValue:(uint8_t)value
{
  NSData *writeData = [NSData dataWithBytes:&value length:sizeof(value)];
  [self.service.peripheral writeValue:writeData
                    forCharacteristic:self.configurationCharacteristic
                                 type:CBCharacteristicWriteWithResponse];
}

- (void)p_setRemoteMode
{
  [self p_writeConfigurationValue:ConfigurationModeRemote];
}

- (void)p_setLocalMode
{
  [self p_writeConfigurationValue:ConfigurationModeLocal];
}

@end
