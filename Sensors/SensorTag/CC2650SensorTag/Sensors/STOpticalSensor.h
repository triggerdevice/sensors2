//
//  STOpticalSensor.h
//  Sensors
//
//  Created by Masahiro Murase on 2015/09/20.
//  Copyright (c) 2015年 TriggerDevice. All rights reserved.
//

#import "STSensor.h"

/*
 * JSON
 *
 * Key-Value
 * optical: 浮動小数点数 // 照度 [lux]
 *
 */

@interface STOpticalSensor : STSensor

/// 照度 [lux]
@property (readonly, nonatomic, strong) NSNumber *optical;


@end
