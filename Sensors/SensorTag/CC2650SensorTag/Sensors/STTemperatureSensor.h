//
//  STTemperatureSensor.h
//  Sensors
//
//  Created by Masahiro Murase on 2015/12/04.
//  Copyright (c) 2015年 TriggerDevice. All rights reserved.
//

#import "STSensor.h"

/*
 * JSON
 *
 * Key-Value
 * AmbTemp: 浮動小数点数  // 気温 [℃]
 * IRTemp: 浮動小数点数   // 対物温度 [℃]
 *
 */

@interface STTemperatureSensor : STSensor

/// 赤外線温度センサーで計測した温度 [℃]
@property (readonly, nonatomic, strong) NSNumber *IRTemp;

/// 環境温度 [℃]
@property (readonly, nonatomic, strong) NSNumber *ambTemp;

@end
