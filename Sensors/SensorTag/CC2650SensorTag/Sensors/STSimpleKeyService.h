//
//  STSimpleKeyService.h
//  Sensors2
//
//  Created by Masahiro Murase on 2015/12/05.
//  Copyright © 2015年 TriggerDevice. All rights reserved.
//

#import "STSensor.h"

@interface STSimpleKeyService : STSensor

/// 左のボタン(User button)の押下状態。0:unpress, 1:pressed
@property (readonly, nonatomic, assign) NSNumber* key1;

/// 右のボタン(Power button)の押下状態。0:unpress, 1:pressed
@property (readonly, nonatomic, assign) NSNumber* key2;

@end
