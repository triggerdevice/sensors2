//
//  STHumiditySensor.h
//  Sensors
//
//  Created by Masahiro Murase on 2015/12/04.
//  Copyright (c) 2015年 TriggerDevice. All rights reserved.
//

#import "STSensor.h"

/*
 * JSON
 *
 * Key-Value
 * humidityTemp: 浮動小数点数 // 温度 [℃]
 * humidity: 浮動小数点数   // 相対湿度 [%rH]
 *
 */

@interface STHumiditySensor : STSensor

/// 相対湿度 [%rH]
@property (nonatomic, strong, readonly) NSNumber *humidity;

/// 温度 [℃]
@property (nonatomic, strong, readonly) NSNumber *humidity_t;

@end
