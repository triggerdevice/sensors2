//
//  STHumiditySensor.m
//  Sensors
//
//  Created by Masahiro Murase on 2015/12/04.
//  Copyright (c) 2015年 TriggerDevice. All rights reserved.
//

#import "STHumiditySensor.h"

@implementation STHumiditySensor

- (NSDictionary *)snapshot
{
  return @{
#if defined(OLD_SENSORTAG_COMPATIBLE_JSON)
           @"relativeHumidity": self.humidity,
#else
           @"humidity": self.humidity,
#//           @"humidityTemp": self.humidity_t,
#endif
           };
}

- (NSNumber *)humidity_t
{
  NSData *rawData = self.dataCharacteristic.value;
  if (!rawData) {
    return @(-0.0);
  }
  
  int16_t value = 0;
  [rawData getBytes:&value range:NSMakeRange(0, sizeof(value))];
  return @([self p_convertRawTemperature:value]);
}

- (NSNumber *)humidity
{
  NSData *rawData = self.dataCharacteristic.value;
  if (!rawData) {
    return @(-0.0);
  }
  
  int16_t value = 0;
  [rawData getBytes:&value range:NSMakeRange(2, sizeof(value))];
  return @([self p_convertRawHumidity:value]);
}

#pragma mark - Conversion algorithm from the raw data to a sensor value

- (float)p_convertRawTemperature:(uint16_t)rawTemperature
{
  uint16_t hoge = 0;
  float fuga = 0.0;
  
  float temperature = 0.0;
  sensorHdc1000Convert(rawTemperature, hoge, &temperature, &fuga);
  
  return temperature;
}

- (float)p_convertRawHumidity:(uint16_t)rawHumidity
{
  uint16_t hoge = 0;
  float fuga = 0.0;
  
  float humidity = 0.0;
  sensorHdc1000Convert(hoge, rawHumidity, &fuga, &humidity);
  
  return humidity;
}

void sensorHdc1000Convert(uint16_t rawTemp, uint16_t rawHum, float *temp, float *hum)
{
  //-- calculate temperature [°C]
  *temp = ((double)rawTemp / 65536) * 165 - 40;
  
  //-- calculate relative humidity [%RH]
  *hum = ((double)rawHum / 65536) * 100;
}

@end
