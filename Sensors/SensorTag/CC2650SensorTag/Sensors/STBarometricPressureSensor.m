//
//  STBarometricPressureSensor.m
//  Sensors
//
//  Created by Masahiro Murase on 2015/12/04.
//  Copyright (c) 2015年 TriggerDevice. All rights reserved.
//

#import "STBarometricPressureSensor.h"

@interface STBarometricPressureSensor ()
@end

@implementation STBarometricPressureSensor

- (NSDictionary *)snapshot
{
  return @{
#if defined(OLD_SENSORTAG_COMPATIBLE_JSON)
           @"pressure": self.baroPres,
#else
           @"baroPres": self.baroPres,
//           @"baroTemp": self.baro_t,
#endif
           };
}

#pragma mark - Accessor

- (NSNumber *)baroPres
{
  NSData *rawData = self.dataCharacteristic.value;
  if (!rawData) {
    return @(-0.0);
  }
  
  int32_t value = 0;
  [rawData getBytes:&value range:NSMakeRange(3, 3)];
  return @(calcBmp280(value));
}

- (NSNumber *)baro_t
{
  NSData *rawData = self.dataCharacteristic.value;
  if (!rawData) {
    return @(-0.0);
  }
  
  int32_t value = 0;
  [rawData getBytes:&value range:NSMakeRange(0, 3)];
  return @(calcBmp280(value));
}

#pragma mark - Conversion algorithm from the raw data to a sensor value

float calcBmp280(uint32_t rawValue)
{
  return rawValue / 100.0f;
}

@end
