//
//  STIOService.h
//  Sensors2
//
//  Created by Masahiro Murase on 2015/12/05.
//  Copyright © 2015年 TriggerDevice. All rights reserved.
//

#import "STSensor.h"

typedef NS_ENUM(NSUInteger, ST2IOServiceIOState) {
  ST2IOServiceIOStateOff = 0,
  ST2IOServiceIOStateOn,
  ST2IOServiceIOStateBlink,
};

@interface STIOService : STSensor

@property (nonatomic, assign) ST2IOServiceIOState redLEDState;
@property (nonatomic, assign) ST2IOServiceIOState greenLEDState;
@property (nonatomic, assign) ST2IOServiceIOState buzzerState;

/**
 *  IDCF Channelのアクションに指定した文字列でLEDとBuzzerを制御する
 *  @parameter payload アクションに書いた文字列
 *  以下のパラメータを１行ずつ指定する
 *  redled-on
 *  redled-off
 *  redled-blink
 *  greenled-on
 *  greenled-off
 *  greenled-blink
 *  buzzer-on
 *  buzzer-off
 *  buzzer-blink
 */
- (void)applyPayloadString:(NSString *)payload;

@end
