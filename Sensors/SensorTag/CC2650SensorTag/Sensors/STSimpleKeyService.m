//
//  STSimpleKeyService.m
//  Sensors2
//
//  Created by Masahiro Murase on 2015/12/05.
//  Copyright © 2015年 TriggerDevice. All rights reserved.
//

#import "STSimpleKeyService.h"

typedef NS_ENUM(uint8_t, DataBit) {
  DataBitUserButton   = 0x01 << 0,
  DataBitPowerButton  = 0x01 << 1,
  DataBitReedRelay    = 0x01 << 2,
};


@interface STSimpleKeyService ()
@end

@implementation STSimpleKeyService

- (NSDictionary *)snapshot
{
  return @{
           @"key1": self.key1,
           @"key2": self.key2,
           };
}

- (NSNumber *)key1
{
  NSData *rawData = self.dataCharacteristic.value;
  if (!rawData) {
    return @0;
  }
  
  uint8_t value = 0;
  [rawData getBytes:&value length:sizeof(value)];

  return ((value & DataBitUserButton) == DataBitUserButton) ? @1 : @0;
}

- (NSNumber *)key2
{
  NSData *rawData = self.dataCharacteristic.value;
  if (!rawData) {
    return @0;
  }
  
  uint8_t value = 0;
  [rawData getBytes:&value length:sizeof(value)];

  return ((value & DataBitPowerButton) == DataBitPowerButton) ? @1 : @0;
}

@end
