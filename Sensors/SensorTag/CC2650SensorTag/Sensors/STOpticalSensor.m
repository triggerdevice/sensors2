//
//  STOpticalSensor.m
//  Sensors
//
//  Created by Masahiro Murase on 2015/12/04.
//  Copyright (c) 2015年 TriggerDevice. All rights reserved.
//

#import "STOpticalSensor.h"

@interface STOpticalSensor ()
@end

@implementation STOpticalSensor

- (NSDictionary *)snapshot
{
  return @{
           @"optical": self.optical,
           };
}

- (NSNumber *)optical
{
  NSData *rawData = self.dataCharacteristic.value;
  if (!rawData) {
    return @(-0.0);
  }
  
  uint16_t value = 0;
  [rawData getBytes:&value length:sizeof(value)];
  return @(sensorOpt3001Convert(value));
}

#pragma mark - Conversion algorithm from the raw data to a sensor value

float sensorOpt3001Convert(uint16_t rawData)
{
  uint16_t e, m;
  
  m = rawData & 0x0FFF;
  e = (rawData & 0xF000) >> 12;
  
  return m * (0.01 * pow(2.0, e));
}
@end
