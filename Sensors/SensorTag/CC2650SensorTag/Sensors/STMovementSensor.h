//
//  STMovementSensor.h
//  Sensors
//
//  Created by Masahiro Murase on 2015/12/03.
//  Copyright (c) 2015年 TriggerDevice. All rights reserved.
//

#import "STSensor.h"

/*
 * JSON
 *
 * Key-Value
 * gyroX: 浮動小数点数  // ジャイロ X軸 [deg/s]
 * gyroY: 浮動小数点数  // ジャイロ Y軸 [deg/s]
 * gyroZ: 浮動小数点数  // ジャイロ Z軸 [deg/s]
 * accX: 浮動小数点数   // 加速度 X軸 [G]
 * accY: 浮動小数点数   // 加速度 Y軸 [G]
 * accZ: 浮動小数点数   // 加速度 Z軸 [G]
 * magX: 浮動小数点数   // 地磁気 X軸 [uT]
 * magY: 浮動小数点数   // 地磁気 Y軸 [uT]
 * magZ: 浮動小数点数   // 地磁気 Z軸 [uT]
 *
 */

// Accelerometer ranges
typedef NS_ENUM(uint16_t, AccelerometerRange) {
  kAccRange2G = 0,
  kAccRange4G = 1,
  kAccRange8G = 2,
  kAccRange16G = 3,
};

@interface STMovementSensor : STSensor

// 範囲は+-250
/// X軸ジャイロ [deg/s]
@property (readonly, nonatomic, strong) NSNumber *gyroX;

/// Y軸ジャイロ [deg/s]
@property (readonly, nonatomic, strong) NSNumber *gyroY;

/// Z軸ジャイロ [deg/s]
@property (readonly, nonatomic, strong) NSNumber *gyroZ;

/// 加速度の範囲。デフォルトでは +/-2G
@property (nonatomic, assign) AccelerometerRange accRange;

/// X軸加速度 [G]
@property (readonly, nonatomic, strong) NSNumber *accX;

/// Y軸加速度 [G]
@property (readonly, nonatomic, strong) NSNumber *accY;

/// Z軸加速度 [G]
@property (readonly, nonatomic, strong) NSNumber *accZ;


// 範囲は+-4900
/// X軸地磁気 [G]
@property (readonly, nonatomic, strong) NSNumber *magX;

/// Y軸地磁気 [G]
@property (readonly, nonatomic, strong) NSNumber *magY;

/// Z軸地磁気 [G]
@property (readonly, nonatomic, strong) NSNumber *magZ;

@end
