//
//  STSensor.h
//  Sensors
//
//  Created by Masahiro Murase on 2015/09/19.
//  Copyright (c) 2015年 TriggerDevice. All rights reserved.
//

@import CoreBluetooth;
#import <Foundation/Foundation.h>

@class STSensorConfiguration;


@interface STSensor : NSObject
@property (readonly, nonatomic, weak) CBService *service;
@property (readonly, nonatomic, weak) CBCharacteristic *dataCharacteristic;
@property (readonly, nonatomic, weak) CBCharacteristic *configurationCharacteristic;
@property (readonly, nonatomic, weak) CBCharacteristic *periodCharacteristic;

/// センサーを有効化する
- (void)enable;

/// センサーを無効化する
- (void)disable;

/// センサー値の更新通知をする/しない
@property (nonatomic, assign, getter=isNotificationEnabled) BOOL notificationEnabled;

/// センサーの生データ
@property (nonatomic, strong) NSData *value;

- (instancetype)initWithService:(CBService *)service configuration:(STSensorConfiguration *)configuration NS_DESIGNATED_INITIALIZER;

/// センサー値の更新間隔
- (void)setPeriod:(float)seconds;

/// 指定されたUUIDのキャラクタリスティックを持っているかどうかを調べる
- (BOOL)hasCharacteristicWithUUID:(NSString *)characteristicUUIDString;

/// 最新のセンサー値を辞書形式で取得する
- (NSDictionary *)snapshot;

/**
 * 現在のセンサー値を起点としてキャリブレーションする。
 * キャリブレーション機能を持たないセンサーでは何も起きない。
 */
- (void)calibrate;

// -- イベント --

/// センサー値の読み出しや更新通知のタイミングで呼ばれる
- (void)sensorDidUpdateValueForCharacteristic:(CBCharacteristic *)characteristic;

/// センサーの初期設定が完了した
- (void)sensorDidFinishConfiguration;


- (instancetype)init __attribute__((unavailable("unavailable initializer")));

@end
