//
//  STTemperatureSensor.m
//  Sensors
//
//  Created by Masahiro Murase on 2015/12/04.
//  Copyright (c) 2015年 TriggerDevice. All rights reserved.
//

#import "STTemperatureSensor.h"

@interface STTemperatureSensor ()
@end

@implementation STTemperatureSensor

- (NSDictionary *)snapshot
{
  return @{
#if defined(OLD_SENSORTAG_COMPATIBLE_JSON)
           @"objctTemperature": self.IRTemp,
           @"ambientTemperature": self.ambTemp,
#else
           @"IRTemp": self.IRTemp,
           @"AmbTemp": self.ambTemp,
#endif
           };
}

- (NSNumber *)IRTemp
{
  NSData *rawData = self.dataCharacteristic.value;
  if (!rawData) {
    return @(-0.0);
  }

  int16_t value;
  [rawData getBytes:&value range:NSMakeRange(0, sizeof(value))];

  return @([self p_convertRawIRTemp:value]);
}

- (NSNumber *)ambTemp
{
  NSData *rawData = self.dataCharacteristic.value;
  if (!rawData) {
    return @(-0.0);
  }
  
  int16_t value;
  [rawData getBytes:&value range:NSMakeRange(2, sizeof(value))];
  
  return @([self p_convertRawAmbTemp:value]);
}

#pragma mark - Conversion algorithm from the raw data to a sensor value

- (float)p_convertRawIRTemp:(uint16_t)rawIRTemp
{
  uint16_t hoge = 0;
  float fuga = 0.0;
  
  float tIR = 0.0;
  sensorTmp007Convert(hoge, rawIRTemp, &fuga, &tIR);
  
  return tIR;
}

- (float)p_convertRawAmbTemp:(uint16_t)rawAmbTemp
{
  uint16_t hoge = 0;
  float fuga = 0.0;

  float tAmb = 0.0;
  sensorTmp007Convert(rawAmbTemp, hoge, &tAmb, &fuga);

  return tAmb;
}

void sensorTmp007Convert(uint16_t rawAmbTemp, uint16_t rawObjTemp, float *tAmb, float *tObj)
{
  const float SCALE_LSB = 0.03125;
  float t;
  int it;
  
  it = (int)((rawObjTemp) >> 2);
  t = ((float)(it)) * SCALE_LSB;
  *tObj = t;
  
  it = (int)((rawAmbTemp) >> 2);
  t = (float)it;
  *tAmb = t * SCALE_LSB;
}

@end
