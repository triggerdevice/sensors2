//
//  STCC2650SensorTag.m
//  Sensors2
//
//  Created by calmscape on 2015/10/09.
//  Copyright © 2015年 TriggerDevice. All rights reserved.
//

#import "STSensorTag+PrivateProperties.h"
#import "STSensor.h"
#import "STCC2650SensorTag.h"

static NSString * const kSensorTagLocalName = @"CC2650 SensorTag";

@implementation STCC2650SensorTag

- (SensorTagType)type
{
  return SensorTagTypeCC2650;
}

+ (BOOL)isSensorTagName:(NSString *)localName
{
  return [localName isEqualToString:kSensorTagLocalName];
}

/* NOTE:
 * SensorTag2ではセンサー値の通知はClient Characteristic Configuration descriptorsに0x0001を書き込むことになっているが、
 * CoreBluetoothでは setNotifyValue:forCharacteristic:を使わないと
 * "Client Characteristic Configuration descriptors must be configured using setNotifyValue:forCharacteristic:"
 * のように例外が発生する。
 * また、Client Characteristic Configuration descriptorsを持つキャラクタリスティックに対してsetNotifyValue:forCharacteristic:を
 * 行うことになるが、Dataキャラクタリスティックに対するsetNotifyと変わらないのでそれで代用する(STSensorクラス内)。
 */

@end
