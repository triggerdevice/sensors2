//
//  STCC2650SensorTag.h
//  Sensors2
//
//  Created by calmscape on 2015/10/09.
//  Copyright © 2015年 TriggerDevice. All rights reserved.
//
//  http://processors.wiki.ti.com/index.php/SensorTag2015
//  http://processors.wiki.ti.com/index.php/CC2650_SensorTag_User's_Guide
//
//  Sensors
//  IR Temperature, both object and ambient temperature
//  Movement, 9 axis (accelerometer, gyroscope, magnetometer)
//  Humidity, both relative humidity and temperature
//  Barometer, both pressure and temperature
//  Optical, light intensity
//

#import "STSensorTag.h"

@interface STCC2650SensorTag : STSensorTag

/// 指定したローカルネームがセンサータグのものかどうか
+ (BOOL)isSensorTagName:(NSString *)localName;

@end
