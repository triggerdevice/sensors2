//
//  STSensorConfiguration.m
//  Sensors
//
//  Created by Masahiro Murase on 2015/09/20.
//  Copyright (c) 2015年 TriggerDevice. All rights reserved.
//

#import "STSensorConfiguration.h"

NSString * const kSensorNameKey = @"sensor";
NSString * const kServiceUUIDKey = @"service";
NSString * const kDataCharacteristicUUIDKey = @"data characteristic";
NSString * const kConfigurationCharacteristicUUIDKey = @"configuration characteristic";
NSString * const kCalibrationCharacteristicUUIDKey = @"calibration characteristic";
NSString * const kPeriodCharacteristicUUIDKey = @"period characteristic";
NSString * const kPeriodKey = @"period";

NSString * const kIRTemperatureSensorNameKey = @"IR Temperature";
NSString * const kAccelerometerSensorNameKey = @"Accelerometer";
NSString * const kMovementSensorNameKey = @"Movement";
NSString * const kHumiditySensorNameKey = @"Humidity";
NSString * const kMagnetometerSensorNameKey = @"Magnetometer";
NSString * const kBarometricPressureSensorNameKey = @"Barometric Pressure";
NSString * const kGyroscopeSensorNameKey = @"Gyroscope";
NSString * const kOpticalSensorNameKey = @"Optical";
NSString * const kIOServiceNameKey = @"IO Service";
NSString * const kSimpleKeyServiceNameKey = @"Simple Keys Service";


// 更新間隔の指定が無かった場合に使うデフォルト値
static const float kDefaultPeriod = 1.0f;

// センサー情報の設定ファイルのフェフォルトファイル名(拡張子なし)
static NSString * const kDefaultSensorSettingsName = @"SensorTagSettings";


@interface STSensorConfiguration ()
@property (nonatomic, copy) NSArray *sensorConfigurations;
@end


@implementation STSensorConfiguration

+ (float)defaultPeriod
{
  return kDefaultPeriod;
}

+ (instancetype)defaultConfiguration
{
  return [self new];
}

- (instancetype)init
{
  NSURL *url = [[NSBundle mainBundle] URLForResource:kDefaultSensorSettingsName withExtension:@"json"];
  return [self initWithContentsOfURL:url];
}

- (instancetype)initWithContentsOfURL:(NSURL *)aURL
{
  self = [super init];
  if (self) {
    NSData *jsonData = [NSData dataWithContentsOfURL:aURL];
    NSError *error = nil;
    NSArray *configurations = [NSJSONSerialization JSONObjectWithData:jsonData
                                                              options:NSJSONReadingMutableContainers
                                                                error:&error];
    if (error) {
      return nil;
    }
    _sensorConfigurations = configurations;
  }
  return self;
}

- (NSDictionary *)dictionaryFromServiceUUID:(NSString *)aUUIDString
{
  for (NSDictionary *configuration in _sensorConfigurations) {
    if ([configuration[kServiceUUIDKey] isEqualToString:aUUIDString]) {
      return configuration;
    }
  }
  return nil;
}

@end
