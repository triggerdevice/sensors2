//
//  AppSettings.h
//  Sensors
//
//  Created by Masahiro Murase on 2015/09/23.
//  Copyright (c) 2015年 TriggerDevice. All rights reserved.
//

#import <Foundation/Foundation.h>

@class MeshbluTrigger;
@class MeshbluAction;

@interface AppSettings : NSObject

@property (readonly, nonatomic, assign) BOOL isTransmissionEnabled;
@property (readonly, nonatomic, assign) NSUInteger transmissionInterval;
@property (readonly, nonatomic, copy) NSString *host;

/// 送り先のトリガー
@property (readonly, nonatomic, strong) MeshbluTrigger *destinationTrigger;

@property (readonly, nonatomic, assign) BOOL action1ForwardingEnabled;
@property (readonly, nonatomic, assign) BOOL action2ForwardingEnabled;
@property (readonly, nonatomic, assign) BOOL action3ForwardingEnabled;
@property (readonly, nonatomic, assign) BOOL action4ForwardingEnabled;
@property (readonly, nonatomic, assign) BOOL action5ForwardingEnabled;

/// enabledかつUUIDの設定があるactionのUUID文字列を配列形式で返す
- (NSArray *)forwardingActionUUIDs;


// Meshblu Devices
@property (readonly, nonatomic, strong) MeshbluTrigger *meshbluTrigger1;
@property (readonly, nonatomic, strong) MeshbluTrigger *meshbluTrigger2;
@property (readonly, nonatomic, strong) MeshbluTrigger *meshbluTrigger3;
@property (readonly, nonatomic, strong) MeshbluTrigger *meshbluTrigger4;
@property (readonly, nonatomic, strong) MeshbluTrigger *meshbluTrigger5;
@property (readonly, nonatomic, strong) MeshbluAction *meshbluAction1;
@property (readonly, nonatomic, strong) MeshbluAction *meshbluAction2;
@property (readonly, nonatomic, strong) MeshbluAction *meshbluAction3;
@property (readonly, nonatomic, strong) MeshbluAction *meshbluAction4;
@property (readonly, nonatomic, strong) MeshbluAction *meshbluAction5;

// IDCF Channel
@property (readonly, nonatomic, strong) MeshbluAction *subscribeAction;

@end
