//
//  IDCFSubscriber.h
//  Sensors2
//
//  Created by Masahiro Murase on 2015/12/06.
//  Copyright © 2015年 TriggerDevice. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^MessageRecieverBlock)(NSData *payload);

@interface IDCFSubscriber : NSObject

@property (nonatomic, strong) NSString *host;
@property (nonatomic, strong) NSString *userName;
@property (nonatomic, strong) NSString *password;
@property (nonatomic, strong) NSString *topic;
@property (nonatomic, copy) MessageRecieverBlock messageRecieverBlock;

- (void)subscribe;
- (void)unsubscribe;

@end
