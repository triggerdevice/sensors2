//
//  AppSettings.m
//  Sensors
//
//  Created by Masahiro Murase on 2015/09/23.
//  Copyright (c) 2015年 TriggerDevice. All rights reserved.
//

#import "MeshbluDevice.h"
#import "AppSettings.h"

// UserDefaults Keys
static NSString * const kEnableTransmissionKey = @"enable_transmission";
static NSString * const kTransmissionIntervalKey = @"transmission_interval";
static NSString * const kHostKey = @"host";

static NSString * const kDestinationTriggerIndexKey = @"destination_trigger_index";
static NSString * const kAction1EnabledKey = @"action-1_forwarding_enabled";
static NSString * const kAction2EnabledKey = @"action-2_forwarding_enabled";
static NSString * const kAction3EnabledKey = @"action-3_forwarding_enabled";
static NSString * const kAction4EnabledKey = @"action-4_forwarding_enabled";
static NSString * const kAction5EnabledKey = @"action-5_forwarding_enabled";

static NSString * const kMeshbluTrigger1UUID = @"meshblu_trigger-1_uuid";
static NSString * const kMeshbluTrigger1Token = @"meshblu_trigger-1_token";
static NSString * const kMeshbluTrigger2UUID = @"meshblu_trigger-2_uuid";
static NSString * const kMeshbluTrigger2Token = @"meshblu_trigger-2_token";
static NSString * const kMeshbluTrigger3UUID = @"meshblu_trigger-3_uuid";
static NSString * const kMeshbluTrigger3Token = @"meshblu_trigger-3_token";
static NSString * const kMeshbluTrigger4UUID = @"meshblu_trigger-4_uuid";
static NSString * const kMeshbluTrigger4Token = @"meshblu_trigger-4_token";
static NSString * const kMeshbluTrigger5UUID = @"meshblu_trigger-5_uuid";
static NSString * const kMeshbluTrigger5Token = @"meshblu_trigger-5_token";
static NSString * const kMeshbluAction1UUID = @"meshblu_action-1_uuid";
static NSString * const kMeshbluAction1Token = @"meshblu_action-1_token";
static NSString * const kMeshbluAction2UUID = @"meshblu_action-2_uuid";
static NSString * const kMeshbluAction2Token = @"meshblu_action-2_token";
static NSString * const kMeshbluAction3UUID = @"meshblu_action-3_uuid";
static NSString * const kMeshbluAction3Token = @"meshblu_action-3_token";
static NSString * const kMeshbluAction4UUID = @"meshblu_action-4_uuid";
static NSString * const kMeshbluAction4Token = @"meshblu_action-4_token";
static NSString * const kMeshbluAction5UUID = @"meshblu_action-5_uuid";
static NSString * const kMeshbluAction5Token = @"meshblu_action-5_token";

static NSString * const kSubscribeAction = @"subscribe_action_index";


@interface AppSettings ()
@property (nonatomic, strong) NSUserDefaults *userDefaults;
@end

@implementation AppSettings

+ (NSDictionary *)settingsDictionaryWithContentsOfURL:(NSURL *)url
{
  NSString *jsonString = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:NULL];
  NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
  
  NSError *error = nil;
  NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingMutableContainers error:&error];

  if (error) {
    return nil;
  }
  return dic;
}

+ (void)initialize
{
  NSURL *settingsURL = [[NSBundle mainBundle] URLForResource:@"RegisterDefaults" withExtension:@"json"];
  NSDictionary *defaults = [self settingsDictionaryWithContentsOfURL:settingsURL];

  [[NSUserDefaults standardUserDefaults] registerDefaults:defaults];
}

- (instancetype)init
{
  self = [super init];
  if (self) {
    _userDefaults = [NSUserDefaults standardUserDefaults];
  }
  return self;
}

#pragma mark - Accessor

- (BOOL)isTransmissionEnabled
{
  return [[NSUserDefaults standardUserDefaults] boolForKey:kEnableTransmissionKey];
}

- (NSUInteger)transmissionInterval
{
  return [[NSUserDefaults standardUserDefaults] integerForKey:kTransmissionIntervalKey];
}

- (NSString *)host
{
  return [[NSUserDefaults standardUserDefaults] stringForKey:kHostKey];
}

- (MeshbluTrigger *)destinationTrigger
{
  NSInteger index = [[NSUserDefaults standardUserDefaults] integerForKey:kDestinationTriggerIndexKey];
  return [self p_findMeshbluTriggerWithTriggerIndex:index];
}

- (BOOL)action1ForwardingEnabled
{
  return [[NSUserDefaults standardUserDefaults] boolForKey:kAction1EnabledKey];
}

- (BOOL)action2ForwardingEnabled
{
  return [[NSUserDefaults standardUserDefaults] boolForKey:kAction2EnabledKey];
}

- (BOOL)action3ForwardingEnabled
{
  return [[NSUserDefaults standardUserDefaults] boolForKey:kAction3EnabledKey];
}

- (BOOL)action4ForwardingEnabled
{
  return [[NSUserDefaults standardUserDefaults] boolForKey:kAction4EnabledKey];
}

- (BOOL)action5ForwardingEnabled
{
  return [[NSUserDefaults standardUserDefaults] boolForKey:kAction5EnabledKey];
}

#pragma mark - Meshblu Devices

- (MeshbluTrigger *)meshbluTrigger1
{
  NSString *uuid = [_userDefaults stringForKey:kMeshbluTrigger1UUID];
  NSString *token = [_userDefaults stringForKey:kMeshbluTrigger1Token];
  
  return [[MeshbluTrigger alloc] initWithUUID:uuid token:token];
}

- (MeshbluTrigger *)meshbluTrigger2
{
  NSString *uuid = [_userDefaults stringForKey:kMeshbluTrigger2UUID];
  NSString *token = [_userDefaults stringForKey:kMeshbluTrigger2Token];
  
  return [[MeshbluTrigger alloc] initWithUUID:uuid token:token];
}

- (MeshbluTrigger *)meshbluTrigger3
{
  NSString *uuid = [_userDefaults stringForKey:kMeshbluTrigger3UUID];
  NSString *token = [_userDefaults stringForKey:kMeshbluTrigger3Token];
  
  return [[MeshbluTrigger alloc] initWithUUID:uuid token:token];
}

- (MeshbluTrigger *)meshbluTrigger4
{
  NSString *uuid = [_userDefaults stringForKey:kMeshbluTrigger4UUID];
  NSString *token = [_userDefaults stringForKey:kMeshbluTrigger4Token];
  
  return [[MeshbluTrigger alloc] initWithUUID:uuid token:token];
}

- (MeshbluTrigger *)meshbluTrigger5
{
  NSString *uuid = [_userDefaults stringForKey:kMeshbluTrigger5UUID];
  NSString *token = [_userDefaults stringForKey:kMeshbluTrigger5Token];
  
  return [[MeshbluTrigger alloc] initWithUUID:uuid token:token];
}

- (MeshbluAction *)meshbluAction1
{
  NSString *uuid = [_userDefaults stringForKey:kMeshbluAction1UUID];
  NSString *token = [_userDefaults stringForKey:kMeshbluAction1Token];
  
  return [[MeshbluAction alloc] initWithUUID:uuid token:token];
}

- (MeshbluAction *)meshbluAction2
{
  NSString *uuid = [_userDefaults stringForKey:kMeshbluAction2UUID];
  NSString *token = [_userDefaults stringForKey:kMeshbluAction2Token];
  
  return [[MeshbluAction alloc] initWithUUID:uuid token:token];
}

- (MeshbluAction *)meshbluAction3
{
  NSString *uuid = [_userDefaults stringForKey:kMeshbluAction3UUID];
  NSString *token = [_userDefaults stringForKey:kMeshbluAction3Token];
  
  return [[MeshbluAction alloc] initWithUUID:uuid token:token];
}

- (MeshbluAction *)meshbluAction4
{
  NSString *uuid = [_userDefaults stringForKey:kMeshbluAction4UUID];
  NSString *token = [_userDefaults stringForKey:kMeshbluAction4Token];
  
  return [[MeshbluAction alloc] initWithUUID:uuid token:token];
}

- (MeshbluAction *)meshbluAction5
{
  NSString *uuid = [_userDefaults stringForKey:kMeshbluAction5UUID];
  NSString *token = [_userDefaults stringForKey:kMeshbluAction5Token];
  
  return [[MeshbluAction alloc] initWithUUID:uuid token:token];
}

- (MeshbluAction *)subscribeAction
{
  NSUInteger actionIndex = [[_userDefaults stringForKey:kSubscribeAction] integerValue];
  NSParameterAssert(0 <= actionIndex && actionIndex <= 5);

  if (actionIndex == 0) {
    return nil;
  }
  return [[self p_allMeshbluActions] objectAtIndex:actionIndex - 1];
}

#pragma mark - Public interfaces

- (NSArray *)forwardingActionUUIDs
{
  NSMutableArray *UUIDs = [NSMutableArray array];
  NSString *uuid = nil;
  
  uuid = [self meshbluAction1].uuid;
  if (self.action1ForwardingEnabled && ![self p_isEmptyString:uuid]) {
    [UUIDs addObject:uuid];
  }
  
  uuid = [self meshbluAction2].uuid;
  if (self.action2ForwardingEnabled && ![self p_isEmptyString:uuid]) {
    [UUIDs addObject:uuid];
  }
  
  uuid = [self meshbluAction3].uuid;
  if (self.action3ForwardingEnabled && ![self p_isEmptyString:uuid]) {
    [UUIDs addObject:uuid];
  }
  
  uuid = [self meshbluAction4].uuid;
  if (self.action4ForwardingEnabled && ![self p_isEmptyString:uuid]) {
    [UUIDs addObject:uuid];
  }
  
  uuid = [self meshbluAction5].uuid;
  if (self.action5ForwardingEnabled && ![self p_isEmptyString:uuid]) {
    [UUIDs addObject:uuid];
  }
  
  return [UUIDs copy];
}

#pragma mark - Private instance methods

- (NSArray *)p_allMeshbluActions
{
  return @[[self meshbluAction1],
           [self meshbluAction2],
           [self meshbluAction3],
           [self meshbluAction4],
           [self meshbluAction5],
           ];
}

- (NSArray *)p_allMeshbluTriggers
{
  return @[[self meshbluTrigger1],
           [self meshbluTrigger2],
           [self meshbluTrigger3],
           [self meshbluTrigger4],
           [self meshbluTrigger5],
           ];
}

// trigger-?
// ?: index
- (MeshbluTrigger *)p_findMeshbluTriggerWithTriggerIndex:(NSUInteger)index
{
  NSArray *meshbluDevices = [self p_allMeshbluTriggers];
  
  if (index == 0 || meshbluDevices.count < index) {
    return nil;
  }
  return meshbluDevices[index-1];
}

- (BOOL)p_isEmptyString:(NSString *)string
{
  return (!string || [string isEqualToString:@""]);
}

@end
