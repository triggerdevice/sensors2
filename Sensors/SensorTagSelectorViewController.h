//
//  SensorTagSelectorViewController.h
//  Sensors
//
//  Created by Masahiro Murase on 2015/09/18.
//  Copyright (c) 2015年 TriggerDevice. All rights reserved.
//

#import <UIKit/UIKit.h>

@class STSensorTag;
@class SensorTagSelectorViewController;

@protocol SensorTagSelectorViewControllerDelegate <NSObject>
@optional
- (void)controller:(SensorTagSelectorViewController *)controller didSelectSensorTag:(STSensorTag *)sensorTag;
- (void)controllerDidTapCloseButton:(SensorTagSelectorViewController *)controller;
@end

@interface SensorTagSelectorViewController : UIViewController

@property (nonatomic, weak) id<SensorTagSelectorViewControllerDelegate> delegate;
@property (nonatomic, weak) STDeviceManager *deviceManager;

@end
