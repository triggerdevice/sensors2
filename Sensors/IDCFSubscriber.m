//
//  IDCFSubscriber.m
//  Sensors2
//
//  Created by Masahiro Murase on 2015/12/06.
//  Copyright © 2015年 TriggerDevice. All rights reserved.
//

@import UIKit;
#import <MQTTKit.h>

#import "STIOService.h"
#import "IDCFSubscriber.h"

@interface IDCFSubscriber ()
@property (nonatomic, strong) MQTTClient *mqttClient;
@property (nonatomic, assign) BOOL isSubscribeRunning;
@end

@implementation IDCFSubscriber

- (instancetype)init
{
  self = [super init];
  if (self) {
    NSUUID *uuid = [NSUUID UUID];
    _mqttClient = [[MQTTClient alloc] initWithClientId:uuid.UUIDString];

    _isSubscribeRunning = NO;
  }
  return self;
}

- (void)dealloc
{
  _messageRecieverBlock = nil;
}

- (void)subscribe
{
  if (_isSubscribeRunning) {
    return;
  }

  self.isSubscribeRunning = YES;
  [self p_subscribeWithUserName:_userName password:_password];
}

- (void)unsubscribe
{
  if (!_isSubscribeRunning) {
    return;
  }

  [_mqttClient disconnectWithCompletionHandler:nil];
  self.isSubscribeRunning = NO;
}

- (void)p_subscribeWithUserName:(NSString *)userName password:(NSString *)password
{
  NSLog(@"%s", __PRETTY_FUNCTION__);
  
  __weak typeof(self) weakSelf = self;
  [_mqttClient setMessageHandler:^(MQTTMessage *message){
    if (weakSelf.messageRecieverBlock) {
      weakSelf.messageRecieverBlock(message.payload);
    }
  }];
  
  _mqttClient.username = userName;
  _mqttClient.password = password;
  
  [_mqttClient connectToHost:_host
           completionHandler:^(MQTTConnectionReturnCode code) {
             if (code == ConnectionAccepted) {
               [_mqttClient subscribe:_topic withCompletionHandler:nil];
             }
           }];
}

#if 0
- (void)p_onRecieveMQTTMessage:(MQTTMessage *)message
{
  NSDictionary * const ioState = @{
                                   @"greenled-off": @(ST2IOServiceIOStateOff),
                                   @"greenled-on": @(ST2IOServiceIOStateOn),
                                   @"greenled-blink": @(ST2IOServiceIOStateBlink),
                                   @"redled-off": @(ST2IOServiceIOStateOff),
                                   @"redled-on": @(ST2IOServiceIOStateOn),
                                   @"redled-blink": @(ST2IOServiceIOStateBlink),
                                   @"buzzer-off": @(ST2IOServiceIOStateOff),
                                   @"buzzer-on": @(ST2IOServiceIOStateOn),
                                   @"buzzer-blink": @(ST2IOServiceIOStateBlink),
                                   };
  
  NSMutableDictionary *dic = [NSJSONSerialization JSONObjectWithData:message.payload
                                                             options:NSJSONReadingAllowFragments error:NULL];
  NSString *payload = dic[@"data"][@"payload"];
  
  __weak typeof(self) weakSelf = self;
  NSArray *components = [payload componentsSeparatedByString:@"\n"];
  
  [components enumerateObjectsUsingBlock:^(NSString *component, NSUInteger idx, BOOL *stop) {
    NSLog(@"%@", component);
    dispatch_async(dispatch_get_main_queue(), ^{
      if ([component hasPrefix:@"greenled"]) {
        weakSelf.sensorTagManager.ioService.greenLEDState = [ioState[component] integerValue];
      }
      else if ([component hasPrefix:@"redled"]) {
        weakSelf.sensorTagManager.ioService.redLEDState = [ioState[component] integerValue];
      }
      else if ([component hasPrefix:@"buzzer"]) {
        weakSelf.sensorTagManager.ioService.buzzerState = [ioState[component] integerValue];
      }
    });
  }];
  
}
#endif
@end
