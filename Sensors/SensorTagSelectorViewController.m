//
//  SensorTagSelectorViewController.m
//  Sensors
//
//  Created by Masahiro Murase on 2015/09/18.
//  Copyright (c) 2015年 TriggerDevice. All rights reserved.
//

#import "STDeviceManager.h"
#import "STSensorTag.h"
#import "SensorTagSelectorViewController.h"

@interface SensorTagSelectorViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *sensorTagImageView;
- (IBAction)cloaseButtonDidTap:(id)sender;
@end


@implementation SensorTagSelectorViewController

- (void)viewDidLoad
{
  [super viewDidLoad];
  // Do any additional setup after loading the view.

  [self p_configureViews];

  __weak typeof(self) weakSelf = self;
  [_deviceManager startScanningWithFoundBlock:^(STSensorTag *sensorTag) {
    [weakSelf.deviceManager stopScanning];
    
    sensorTagConnectionSuccessBlock successBlock = ^(STSensorTag *sensorTag){
      if ([_delegate respondsToSelector:@selector(controller:didSelectSensorTag:)]) {
        [_delegate controller:weakSelf didSelectSensorTag:sensorTag];
      }
    };
    sensorTagConnectionFailureBlock failureBlock = ^(NSError *error){
      NSLog(@"connect error:%@", [error localizedDescription]);
    };
    [weakSelf.deviceManager connectSensorTagWithSuccessBlock:successBlock failureBlock:failureBlock];
  }];
}

- (void)didReceiveMemoryWarning
{
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

#pragma mark - Private methods
- (void)p_configureViews
{
  _sensorTagImageView.animationImages = @[[UIImage imageNamed:@"SensorTag_switch0"],
                                          [UIImage imageNamed:@"SensorTag_switch1"],];
  _sensorTagImageView.animationDuration = 1.0;
  [_sensorTagImageView startAnimating];
}

#pragma mark - Actions

- (IBAction)cloaseButtonDidTap:(id)sender
{
  if ([_delegate respondsToSelector:@selector(controllerDidTapCloseButton:)]) {
    [_delegate controllerDidTapCloseButton:self];
  }
}
@end
