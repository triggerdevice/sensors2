//
//  SensorValueTransmitter.m
//  Sensors
//
//  Created by Masahiro Murase on 2015/09/23.
//  Copyright (c) 2015年 TriggerDevice. All rights reserved.
//

#import "AFNetworking.h"
#import "SensorValueTransmitter.h"

@interface SensorValueTransmitter ()
@end

@implementation SensorValueTransmitter

- (instancetype)initWithHost:(NSString *)host triggerUUID:(NSString *)triggerUUID triggerToken:(NSString *)triggerToken devices:(NSArray *)devices
{
  self = [super init];
  if (self) {
    _host = host;
    _triggerUUID = triggerUUID;
    _triggerToken = triggerToken;
    _devices = devices;
  }
  return self;
}

- (void)postSensorValues:(NSDictionary *)values success:(void (^)(void))successBlock failure:(void (^)(NSError *error))failureBlock
{
  // HTTPS通信でMeshbluにデータをPOSTする
  
  NSString *httpsURLString = [NSString stringWithFormat:@"http://%@", _host];
  NSURL *hostURL = [NSURL URLWithString:httpsURLString];
  AFHTTPRequestOperationManager *manager = [[AFHTTPRequestOperationManager alloc] initWithBaseURL:hostURL];

  // オレオレ証明書を通すための設定
  manager.securityPolicy.allowInvalidCertificates = YES;
  manager.securityPolicy.validatesDomainName = NO;

  manager.requestSerializer = [AFJSONRequestSerializer serializer];
  [manager.requestSerializer setValue:_triggerUUID forHTTPHeaderField:@"meshblu_auth_uuid"];
  [manager.requestSerializer setValue:_triggerToken forHTTPHeaderField:@"meshblu_auth_token"];
  
  NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
  [parameters setObject:_devices forKey:@"devices"];
  [parameters setObject:values forKey:@"payload"];

  [manager POST:@"/messages"
     parameters:parameters  // nilでもエラーは出ないが発火はしない
        success:^(AFHTTPRequestOperation *operation, id responseObject) {
          if (successBlock) {
            successBlock();
          }
        } failure:^(AFHTTPRequestOperation *operation, NSError *error){
          NSLog(@"error: %@", [error localizedDescription]);
          if (failureBlock) {
            failureBlock(error);
          }
        }];
}

@end
