# Sensors2 #

Sensorsからの主な変更点

* SensorTag2専用
* myThingsのIDCFチャンネルのアクション情報でSensorTag2のLEDとBuzzerを制御する機能を追加  
監視できるアクションは一つ。  
制御できる内容はLED,の点灯、消灯、ブリンク。  
(Buzzerの発音、消音、断続発音)

## アクションとして利用するための設定手順 ##

### アプリの設定 ###
Sensors2アプリの設定画面から、[Subscribe]グループにある[IDCF Channel]で監視するアクションを指定します。

### myThingsの設定 ###
myThingsのIDCFチャンネルをアクションとして指定し、アクション詳細設定で[メッセージ]に以下のメッセージ仕様で文字列を指定します。
 
### メッセージ仕様 ###

メッセージとして指定できる文字列の仕様(EBNF)は以下の通り。

```
action message = { message, newline };
message = prefix, "-", suffix;
prefix = red led | green led | buzzer;
suffix = on | off | blink;
red led = "redled";
green led = "greenled";
buzzer = "buzzer";
on = "on";
off = "off";
blink = "blink";
newline = ? newline character ?;
```

例

```
greenled-blink
buzzer-on
```

指定できるメッセージ一覧
*  redled-on
*  redled-off
*  redled-blink
*  greenled-on
*  greenled-off
*  greenled-blink
*  buzzer-on
*  buzzer-off
*  buzzer-blink

### 注意事項 ###
点灯や発音動作中のLED/ブザーをリセットするためのUIが無いため、Powerボタン３秒押し(長く押しすぎてもダメ)でSensorTagを切断することで代用してください。
